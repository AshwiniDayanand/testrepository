#!/bin/bash

# Collect information about an Ingres installation,  Linux version.
# - Version information
# - Error log information for a specific time period
# - IMA based performance information
# - Catalog Overflow
# collect IMA tables snapshot in comma delimited text files.  Prepend
# Customer Name, Host Name, install_id, and timestamp columns to each table,
# the timestamp value is the same for all rows in all tables and functions 
# as a snapshot key
# The first argument is required and is a customer identifying string.  Only used for multi-tenant databases.
# Optional arguments to collect database specific information.  If not supplied, no databases are touched.
# Assumes the II_SYSTEM environment is already set up.

ProgPath=$(dirname $0)
echo ${II_SYSTEM:?Cannot proceed, II_SYSTEM is not set}
DOMAIN=${1?-"1st argument is a customer name"}
MONTH=${2?-"2nd argument is the month to report, in Ingres date format (i.e. Jan)"}
YEAR=${3?-"3rd argument is the year to report in 4 digitis (i.e. 2018)"}
# Remaining arguments are database names
shift 3
HOST=$(hostname)
IID=$(ingprenv II_INSTALLATION)
CUR_FS=`date +"%Y%m%d_%H%M%S"`

mkdir $HOST.$IID.$CUR_FS
cd $HOST.$IID.$CUR_FS

cp $II_SYSTEM/ingres/version.* $II_SYSTEM/ingres/files/config.dat $II_SYSTEM/ingres/files/protect.dat .

( cd $II_SYSTEM/ingres/files ; find . -name "errlog*" -mtime -30 -print0 2>/dev/null | xargs -0 ls -tr|xargs cat | LC_ALL=C grep " $MONTH .* $YEAR ") >$YEAR-$MONTH.errlog

infodb >infodb.txt

# Below there is a seperate select statement for the title blocks so that an import
# into a spreadsheet needs little or no editing.  SQL92 is rarely used so normally Ingres
# would lower case all the quoted column names.
sql -s imadb <<EOF
\silent
\notitles
\vdelim ,
\trim
\continue
\r
execute procedure ima_set_vnode_domain\g

-- server connect limits
\p\g
\redir Ops Connection Information.csv
select
	'Server', 
	'Name',
        'Max Connections', 
	'Highwater Connections',
        '%Max'
;
select
	sp.server as "Server",
	sp.startup_name as "Name",
        sp.max_connections as "Max Connections",
	ss.highwater_connections as "Highwater Connections",
        (float4(ss.highwater_connections) / float4(sp.max_connections)) * 100.0 as "%Max"
from	ima_dbms_server_parameters sp join ima_dbms_server_status ss on sp.server = ss.server
order by "%Max" desc
\g
-- overall hit rate
-- Use the ema version of the stats table which has counters as int8 instead of int4
\p\g
\redir Ops Hit Rate.csv
select  
	'Server', 
	'Name', 
	'Page Size', 
	'Hits', 
	'Fix',
	'%Hit'
;
select  
	sp.server as "Server",
	sp.startup_name as "Name",
	cs.page_size as "Page Size",
	cs.hit_count as "Hits",
	cs.fix_count as "Fix",
	case when cs.fix_count=0 then 0 else float8(cs.hit_count)/float8(cs.fix_count) * 100 end as "%Hit"
from    ima_dbms_server_parameters sp join ema_dmf_cache_stats cs on sp.server= cs.server
where fix_count>0
order by "%Hit" asc
\g
-- cache reads
\p\g
\redir Ops Cache Reads.csv
select
	'Page Size', 
	'Buffer Count',
	'Group Count', 
	'Group Size', 
	'Page Reads', 
	'Group Buffer Reads',
	'Group Page Reads', 
	'%Group Read'
;
with dmf_rio as (
         select sp.server, sp.startup_name,sp.capabilities, cs.page_size, cs.buffer_count, cs.group_buffer_count,
                cs.group_buffer_size, cs.read_count as page_reads,cs.group_buffer_read_count as group_buf_reads,
                cs.group_buffer_read_count * cs.group_buffer_size as group_page_reads
         from ima_dbms_server_parameters sp
         join ima_dmf_cache_stats cs on sp.server = cs.server
)
select distinct  
	  page_size as "Page Size", 
	  buffer_count as "Buffer Count",
	  group_buffer_count as "Group Count", 
	  group_buffer_size as "Group Size", 
	  page_reads as "Page Reads", 
	  group_buf_reads as "Group Buffer Reads",
	  group_page_reads as "Group Page Reads", 
	  case when page_reads+group_page_reads=0 then 0 else decimal((float8(group_page_reads) / float8(page_reads + group_page_reads)*100),6,2) end as "%Group Read"
from     dmf_rio
where    group_page_reads > 0 or page_reads>0
order by "%Group Read" desc
\g
-- deadlocks
\p\g
\redir Ops Deadlocks.csv
select
	'Transactions',
	'Deadlocks',
	'%Deadlock'
;
select
	lgs.lgd_stat_begin as "Transactions",
	(lks.lkd_stat_deadlock + lks.lkd_stat_cvt_deadlock) as "Deadlocks",
	decimal(float8((lks.lkd_stat_deadlock + lks.lkd_stat_cvt_deadlock)) / float8(lgs.lgd_stat_begin) * 100.0,6,2) as "%Deadlock"
from	ima_lkmo_lkd_stat lks join ima_lgmo_lgd lgs on lks.vnode=lgs.vnode
order by "%Deadlock","Deadlocks" desc
\g
-- lock waits
\p\g
\redir Ops Lockwaits.csv
select	'Lock Requests',
	'Lock Waits',
	'%Wait'
;
select	lkd_stat_request_new + lkd_stat_request_cvt as "Lock Requests",
	lkd_stat_wait + lkd_stat_convert_wait as "Lock Waits",
	decimal(((float8(lkd_stat_wait+lkd_stat_convert_wait)/float8(lkd_stat_request_new+lkd_stat_request_cvt))*100.0),6,2) as "%Wait"
from	ima_lkmo_lkd_stat
order by "%Wait" desc
\g
-- log buffer waits
\p\g
\redir Ops Log Buffer Waits.csv
select	'Log Writes', 
	'Free Waits',
	'Split Waits',
        '%Wait'
;
select	lgd_stat_write as "Log Writes", 
	lgd_stat_free_wait as "Free Waits",
	lgd_stat_splitbuf_wait as "Split Waits",
        decimal(float8(lgd_stat_free_wait+lgd_stat_splitbuf_wait) / float8(lgd_stat_write) * 100,6,2) as "%Wait"
from	ima_lgmo_lgd
order by "%Wait" desc
\g
-- logwriters busy
\p\g
\redir Ops Logwriters Busy.csv
select
	'Writes', 
	'Busy',
	'Writers',
	'%Busy'
;
select
	lgd_stat_writeio as "Writes", 
	lgd_stat_logwriters_busy as "Busy",
	lgd_n_logwriters as "Writers",
	decimal(float8(lgd_stat_logwriters_busy)/float8(lgd_stat_writeio)*100,6,2) as "%Busy"
from	ima_lgmo_lgd
order by "%Busy" desc
\g
-- buffer utilization
\p\g
\redir Ops Log Buffer Utilization.csv
select
	'Kbytes Written',
	'Writes',
	'Block Size',
	'%Used'
;
select
	lgd.lgd_stat_kbytes as "Kbytes Written",
	lgd.lgd_stat_writeio as "Writes",
	lfb.lfb_hdr_lgh_size as "Block Size",
	decimal(((float8(lgd.lgd_stat_kbytes) / float8(lgd.lgd_stat_writeio)) / float8(lfb.lfb_hdr_lgh_size/1024)) * 100,6,2) as "%Used"
from ima_lgmo_lgd lgd
	join ima_lgmo_lfb lfb on lgd.vnode=lfb.vnode
order by "%Used" desc
\g
EOF

# Collect database statistics

for DB in $*
do
sql -s $DB <<EOF
\silent
\notitles
\vdelim ,
\trim
\continue
\r
-- Tables with Overflow
\p\g
\redir $DB Overflow.csv
select
	'Owner',
	'Table',
	'Structure',
	'Pages',
	'Overflow',
	'%Overflow'
;
select
	table_owner as "Owner",
	table_name as "Table",
	storage_structure as "Structure",
	number_pages as "Pages",
	overflow_pages as "Overflow",
	overflow_pages*100/number_pages as "%Overflow"
from iitables
where storage_structure!='HEAP' and (overflow_pages*100/number_pages)>0
order by "%Overflow" desc,1,2
\g
-- tables not journaled
\p\g
\redir $DB Not Journaled.csv
select	
	'Owner',
	'Table',
	'Pages'
;
select	
	table_owner as "Owner",
	table_name as "Table",
	number_pages as "Pages"
from iitables
where     is_journalled = 'N' and table_type='T'
order by 1,2
\g
-- tables without statistics
\p\g
\redir $DB No Statistics.csv
select	
	'Owner',
	'Table',
	'Structure',
	'Pages'
;
select	
	table_owner as "Owner",
	table_name as "Table",
	storage_structure as "Structure",
	number_pages as "Pages"
from iitables
where     table_stats = 'N' 
	and table_type = 'T' 
	and system_use = 'U'
	and number_pages > 1000
order by "Pages",1,2 desc;
\g
-- page utilization
\p\g
\redir $DB Page Utilization.csv
select	
	'Owner',
	'Table',
	'Modified',
	'Size',
	'Pages',
	'Rows',
	'TPP',
	'Avg TPP',
	'%Data Fill',
	'%Actual'
;
select	
	table_owner as "Owner",
	table_name as "Table",
	modify_date as "Modified",
	table_pagesize as "Size",
	number_pages as "Pages",
	num_rows as "Rows",
	tups_per_page as "TPP",
	round(float4(num_rows)/float4(number_pages),1) as "Avg TPP",
	table_dfillpct as "%Data Fill",
	round(float4(num_rows)/float4(number_pages)/float4(tups_per_page)*100.0,1) as "%Actual"
from iitables
where	table_type = 'T' 
	and system_use = 'U' 
	and number_pages > 1000
order by "%Actual",1,2
\g
-- tables nearing max number of pages
\p\g
\redir $DB Max Tagle Page Count.csv
select	
	'Owner',
	'Table',
	'Modified',
	'Size',
	'Pages'
;
select	
	table_owner as "Owner",
	table_name as "Table",
	modify_date as "Modified",
	table_pagesize as "Size",
	number_pages as "Pages"
from iitables
where	table_type = 'T' 
	and system_use = 'U' 
	and number_pages > 7*1024*1024
order by 5,1,2
\g
EOF
done

# Summarize error log items.  The intention is to provide basic monthly report materials.
# For detailed analysis use "anerrlog" to search for patterns and diagnostics.

# Convince sed that we are not processing Unicode in case there are garbage characters in the error log
export LC_ALL=C

# Some codes are ignored because they are always accompanied by lower level DMF errors
# that actually contain useful information.  The sed list is sorted for easy combination of patternsx1
# Also there are some error log lines that do not conform to the Ingres "standard" and
# have to be processed specially.
sed \
    -e '/The database may be marked inoperable.  This can occur if/d' \
        -e '/An exclusive database lock may be held by another session.$/d' \
	    -e '/The database may be open by an exclusive (.SOLE) DBMS server.$/d' \
	        -e '/Associated error messages which provide more detailed information about the problem can be found in the error log/d' \
		    -e '/End Of ingcheck processing/d' \
		        -e '/_SHUTDOWN/d' \
			    -e '/_STARTUP/d' \
			        -e '/ ]: [L]*Query:/d' \
				    -e '/CL100[24A]/d' \
				        -e '/I_DM1405/d' \
					    -e '/CL2518/d' \
					        -e '/CL2530/d' \
						    -e '/CLFE0[567]/d' \
						        -e '/DM004[2B]/d' \
							    -e '/DM005B/d' \
							        -e '/DM0065/d' \
								    -e '/DM008B/d' \
								        -e '/DM105D/d' \
									    -e '/DM9063/d' \
									        -e '/DM9[78]15/d' \
										    -e '/DM9850/d' \
										        -e '/GC000[EB1]/d' \
											    -e '/GC0023/d' \
											        -e '/GC0032/d' \
												    -e '/GC013[89]/d' \
												        -e '/GC015[12]/d' \
													    -e '/GC200[26]/d' \
													        -e '/GC220[5B]/d' \
														    -e '/GC2815/d' \
														        -e '/GC2A10/d' \
															    -e '/GC480[237]/d' \
															        -e '/OP04C0/d' \
																    -e '/QE002[2A]/d' \
																        -e '/QE007[CD]/d' \
																	    -e '/RD000C/d' \
																	        -e '/RE000[12]/d' \
																		    -e '/SC012[389]/d' \
																		        -e '/SC0235/d' \
																			    -e '/SC0271/d' \
																			        -e '/SC051[DE]/d' \
																				    -e '/UL0404/d' \
																				        -e '/US000[23DB]/d' \
																					    -e '/US0010/d' \
																					        -e '/VW1198/d' \
																						    -e '/^$/d' \
																						        $YEAR-$MONTH.errlog | \
																							awk '/DMA031/	{
																								theline=$0;
																									sub(".*lkshow.c:[0-9]*  *.* 20[12][0-9] ","",theline);
																										split(theline,thecontent);
																											errorcount[thecontent[1]]++;
																												next;
																												}
																												/SIGBUS/ {
																													errorcount["SIGBUS"]++;
																														next;
																														}
																														/SIGSEGV/ {
																															errorcount["SIGSEGV"]++;
																																next;
																																}
																																/SIGABRT/ {
																																	errorcount["SIGABRT"]++;
																																		next;
																																		}
																																			{
																																				theline=$0;
																																					subs = sub(".*]: [A-Z][a-z][a-z] .* [0-9][0-9]:[0-9][0-9]:[0-9][0-9] 20[12][0-9] ","",theline);
																																						if ( subs == 0 ) { next; }
																																							split(theline,thecontent);
																																							# Example debug line
																																								if ( thecontent[1] == "Abort" ) {print "DEBUG: ",$0;print "DEBUG: ",theline}
																																									errorcount[thecontent[1]]++;
																																										# Accumulate deadlock specific data
																																											if ( thecontent[1] ~ /DM9042/ ) {deadlocks[thecontent[12],thecontent[9]]++;}
																																												else if ( thecontent[1] ~ /DM9044/ ) {deadlocks[thecontent[15],thecontent[12]]++;}
																																													else if ( thecontent[1] ~ /DM9045/ ) {deadlocks[thecontent[9],thecontent[6]]++;}
																																														else if ( thecontent[1] ~ /DM905C/ ) {deadlocks[thecontent[12],thecontent[9]]++;}
																																															else if ( thecontent[1] ~ /RD002A/ ) {deadlocks["(unknown db)","system catalogs"]++;}
																																																else if ( theline ~ /DM9043.* TABLE lock/ ) {
																																																	    if ( thecontent[10] == ".TABLE_ID" ) {
																																																	    		sub("\\(","",thecontent[11]);
																																																					sub(",.*$","",thecontent[11]);
																																																							locktimeouts[thecontent[14],thecontent[11]]++;
																																																								    } else {
																																																								    		locktimeouts[thecontent[13],thecontent[10]]++;
																																																											    }
																																																											    	}
																																																													else if ( theline ~ /DM9043.* CONTROL lock/ ) {
																																																														    if ( thecontent[10] ~ ".TABLE_ID" ) {
																																																														    	        sub(".TABLE_ID","",thecontent[10]);
																																																																		locktimeouts[thecontent[13],thecontent[10]]++;
																																																																			    } else {
																																																																			    		locktimeouts[thecontent[13],thecontent[10]]++;
																																																																						    }
																																																																						    	}
																																																																								else if ( thecontent[1] ~ /DM9043/ ) {locktimeouts[thecontent[14],thecontent[11]]++;}
																																																																									else if ( thecontent[1] ~ /DM9044/ ) {escalations[thecontent[15],thecontent[12]]++;}
																																																																										else if ( thecontent[1] ~ /DM9041/ ) {escalations[thecontent[18],thecontent[15]]++;}
																																																																										}
																																																																										END	{
																																																																										# Seems like awk does not like spaces in the pipe file name
																																																																											error_file="errlog.errors.csv";error_pipe="sort -n -r -k2 -t, >>"error_file;
																																																																												deadlock_file="errlog.deadlocks.csv";deadlock_pipe="sort -n -r -k3 -t, >>"deadlock_file;
																																																																													timeout_file="errlog.timeouts.csv";timeout_pipe="sort -n -r -k3 -t, >>"timeout_file;
																																																																														escalation_file="errlog.escalations.csv";escalation_pipe="sort -n -r -k3 -t, >>"escalation_file;
																																																																															print("=== Creating errors.csv ===");
																																																																																print "Error Code,Count" >error_file;
																																																																																	for (e in errorcount) {
																																																																																		    printf("%s,%d\n",e,errorcount[e])  | error_pipe ;
																																																																																		    	}
																																																																																				close(error_pipe);
																																																																																					print("=== Creating deadlocks.csv ===");
																																																																																						print "Database,Table,Deadlocks" >deadlock_file;
																																																																																							for (deadlock in deadlocks) {
																																																																																								    split(deadlock, separate, SUBSEP)
																																																																																								    	    printf("%s,%s,%d\n",separate[1],separate[2],deadlocks[deadlock]) | deadlock_pipe ;
																																																																																									    	}
																																																																																											close(deadlock_pipe);
																																																																																												print("=== Creating timeouts.csv ===");
																																																																																													print "Database,Table,Timeouts" >timeout_file;
																																																																																														for (timeout in locktimeouts) {
																																																																																															    split(timeout, separate, SUBSEP)
																																																																																															    	    printf("%s,%s,%d\n",separate[1],separate[2],locktimeouts[timeout]) |timeout_pipe ;
																																																																																																    	}
																																																																																																		close(timeout_pipe);
																																																																																																			print("=== Creating escalations.csv ===");
																																																																																																				print "Database,Table,Escalations" >escalation_file;
																																																																																																					for (escalation in escalations) {
																																																																																																						    split(escalation, separate, SUBSEP)
																																																																																																						    	    printf("%s,%s,%d\n",separate[1],separate[2],escalations[escalation]) | escalation_pipe ;
																																																																																																							    	}
																																																																																																									close(escalation_pipe);
																																																																																																									}'

																																																																																																									# Specialty stuff.  Mostly Does not work prior to 9.X
																																																																																																									# Nasty bits
																																																																																																									echo == Signals ==
																																																																																																									grep SIG $YEAR-$MONTH.errlog | sed -e 's/^.*]: //' -e 's/The transaction.* in //' -e 's/is being.*$//'
																																																																																																									# Force Aborts
																																																																																																									echo == Force Aborts ==
																																																																																																									grep E_DM9059_TRAN_FORCE_ABORT $YEAR-$MONTH.errlog | sed -e 's/^.*]: //' -e 's/The transaction.* in //' -e 's/is being.*$//'
																																																																																																									# System startups
																																																																																																									echo == Startups ==
																																																																																																									egrep 'GC0151|SC0129|GC2815|JD0106' $YEAR-$MONTH.errlog | sed -e 's/^.*]: //' -e 's/\([0-9][0-9]:[0-9][0-9]\):[0-9][0-9]/\1/' -e 's/: class.*$//'
																																																																																																									# System shutdowns
																																																																																																									echo == Shutdowns ==
																																																																																																									egrep '_SHUTDOWN|_SERVER_DOWN|DMA469' $YEAR-$MONTH.errlog | sed -e 's/^.*]: //' -e 's/\([0-9][0-9]:[0-9][0-9]\):[0-9][0-9]/\1/' -e 's/: class.*$//'
																																																																																																									# iimonitor session removals
																																																																																																									echo == iimonitor ==
																																																																																																									grep I_SC051E_IIMONITOR_CMD $YEAR-$MONTH.errlog | sed -e 's/^.*]: //' -e '/set server shut/d'

cd ..
tar zcf $HOST.$IID.$CUR_FS.tgz $HOST.$IID.$CUR_FS

sftp $DOMAIN@ftp.actian.com <<EOF
put $HOST.$IID.$CUR_FS.tgz
quit
EOF

echo "Files are generated and uploaded to SFTP successfully!"